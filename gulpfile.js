var gulp       = require('gulp');
//var uglify     = require('gulp-uglify');
var concat     = require('gulp-concat');
//var jshint     = require('gulp-jshint');
var less       = require('gulp-less');
var watch      = require('gulp-watch');
var minifyCSS  = require('gulp-minify-css');
var rename     = require('gulp-rename');
var header     = require('gulp-header');
var symlink    = require('gulp-symlink');
//var jquery     = require('gulp-jquery');
var pkg        = require('./package.json');

var portalBundlePath = 'src/portalBundle/assets';
var bootstrapPath = 'node_modules/bootstrap/dist/';

/* Prepare banner text */
var banner = ['/**',
    ' * <%= pkg.name %> v<%= pkg.version %>',
    ' * <%= pkg.description %>',
    ' * <%= pkg.author.name %> <<%= pkg.author.email %>>',
    ' */',
    ''].join('\n');

gulp.task('compile-portal-less', function() {
    gulp.src(portalBundlePath + '/less/init.less')
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(header(banner, {pkg: pkg}))
        .pipe(rename('portal.min.css'))
        .pipe(gulp.dest('web/assets/css'));
});

gulp.task('bootstrap', function () {
    gulp.src(bootstrapPath + 'css/bootstrap.min.css')
        .pipe(concat('bootstrap.min.css'))
        .pipe(gulp.dest('web/assets/css'));
    gulp.src(bootstrapPath + 'js/bootstrap.min.js')
        .pipe(concat('bootstrap.min.js'))
        .pipe(gulp.dest('web/assets/js'));
});

gulp.task('jquery', function () {
    gulp.src('node_modules/jquery/dist/jquery.min.js')
        .pipe(concat('jquery.min.js'))
        .pipe(gulp.dest('web/assets/js'));
});

gulp.task('compile-portal-js', function() {
    gulp.src(portalBundlePath + '/javascript/*.js')
        .pipe(concat('portal.min.js'))
        .pipe(gulp.dest('web/assets/js'));
});

gulp.task('watching', function() {
    gulp.watch(portalBundlePath + '/less/*.less' , ['compile-portal-less']);
    gulp.watch(portalBundlePath + '/javascript/*.js' , ['compile-portal-js']);
});

gulp.task('build', ['jquery', 'compile-portal-js','bootstrap', 'compile-portal-less']);
gulp.task('watch', ['watching']);
gulp.task('public', ['symlink']);
