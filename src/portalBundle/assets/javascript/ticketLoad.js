ticketLoader = {
    initialize: function (element) {
        this.container = element;
        this.jsonPath = this.container.data('json-path');
        this.ticketsWrapper = this.container.find('.tickets-wrapper');
        this.navTab = this.container.find('.nav-tab');

        this.ticketDataProcessing();
        this.registerEventHandler();

    },

    registerEventHandler: function() {
        var module = this;

        module.navTab.find('a').on('click', function(e) {
            e.preventDefault();
            module.navTab.find('a').removeClass("active");
            $(this).addClass("active");
            module.ticketDataProcessing();
        });

    },

    ticketDataProcessing: function() {
        var module = this;
        var jsonData = module.jsonLoader(module.setActiveTicketName());
        var oneTicket = module.ticketsWrapper.find('.ticket-element').first().clone();

        module.ticketsWrapper.find('.ticket-element').remove();

        $.each(jsonData, function (key, data) {

            oneTicket.find('h2').text(data.title);
            oneTicket.find('.old-price').text(data.oldPrice);
            oneTicket.find('.price').text(data.price);
            oneTicket.find('.time-left span').text(data.timeLeft);

            module.setBasketView(oneTicket, data);
            module.setPriceInfo(oneTicket, data);
            module.setService(oneTicket, data);
            module.setInfoList(oneTicket, data);

            module.ticketsWrapper.append(oneTicket);

            var height = oneTicket.find('.ticket-left').height();
            oneTicket.find('.ticket-right').css('height', height + 'px');
            oneTicket = module.ticketsWrapper.find('.ticket-element').first().clone();

        });


    },

    setBasketView: function(oneTicket, data) {

        if(data.basketButton) {
            oneTicket.find('.basket-functions').show();
            oneTicket.find('.basket-info').hide();
        } else {
            oneTicket.find('.basket-functions').hide();
            oneTicket.find('.basket-info').show();
            oneTicket.find('.basket-info').text('Coming soon!');
        }

    },

    setService: function(oneTicket, data) {
        var servicesWrapper = oneTicket.find('.services-wrapper');
        var oneService = oneTicket.find('.service').first().clone();
        servicesWrapper.find('.service').remove();



        $.each(data.services, function (key, services) {
            oneService.find('.service-icon').removeClass().addClass('service-icon');

            oneService.find('.service-title').text(services.title);
            oneService.find('.service-icon').addClass(services.icon);

            servicesWrapper.append(oneService);
            oneService = oneTicket.find('.service').first().clone();
        });

    },

    setPriceInfo: function(oneTicket, data) {
        oneTicket.find('.prices-info li').remove();
        $.each(data.pricesInfo, function (key, info) {
            oneTicket.find('.prices-info ul').append('<li>'+ info +'</li>');
        });
    },

    setInfoList: function(oneTicket, data) {
        oneTicket.find('.info-list li').remove();
        $.each(data.infoList, function (key, info) {
            oneTicket.find('.info-list ul').append('<li>'+ info +'</li>');
        });
    },

    setActiveTicketName: function() {
        var module = this;
        var ticket = module.navTab.find('.active').data('ticket');

        return ticket;

    },

    jsonLoader: function(ticket) {
        var module = this;
        var json = null;

        $.ajax({
            'async': false,
            'global': false,
            'url': module.jsonPath + ticket + '.json',
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });

        return json;

    }

};

vc.attach('#content', ticketLoader, {eager: true});
