var vc = {config: typeof vc_config == 'object' ? vc_config : {}};

(function(undefined) {
    var filtered = /(.*):(?:on([\S]+)$)/; // selector, event
    var registry = []; // eager behaviorok, a kesobbi reattach miatt elrakva
    var debug = typeof vc_debug !== 'undefined';


    // onReady-ra attach
    var attachQueue = [];
    jQuery(function() {
        jQuery.each(attachQueue, function(i, value) {
            attach.apply(null, value);
        });
        attachQueue = null;
    });

    // eagerLoad
    jQuery(document).bind('elementInsert', eagerLoad);
    // eager bind to elements present after domReady
    jQuery(eagerLoad);

    /**
     * @param [root] ehhez az elemhez lesz kötve a delegate handler (default: body elem)
     * @param selector {String|jQuery}
     * @param behavior
     * @param options
     */
    vc.attach = function(root, selector, behavior, options) {
        // fix params
        // optional root
        if(typeof selector !== 'string' && typeof selector.selector === 'undefined') {
            options = behavior;
            behavior = selector;
            selector = root;
            root = 'body';
        }
        // selector == jQuery object
        if(typeof selector.selector !== 'undefined') {
            //TODO: lehessen plain dom elem is
            root = selector;
            selector = undefined;
        }
        options = options || {};

        // prepare
        var events = {}; // events : (filter || '') -> event.type -> handler
        jQuery.each((behavior.handlers || behavior), function(member) {
            var parts, handler = behavior.handlers ? behavior[behavior.handlers[member]] : behavior[member];
            if(member.indexOf('on') === 0 && member.length > 2) { // onclick: function() {} case
                ( events[''] || (events['']={}) )[ member.substring(2) ] = handler; // events['']['click'] = function() {}
            } else if((parts = member.match(filtered))) { // .myClass:onclick: function() {} case
                ( events[parts[1]] || (events[parts[1]]={}) )[ parts[2] ] = handler; // events['.myClass']['click'] = function() {}
            }
        });

        if(options.eager) {
            // eager + bind
            if(selector !== undefined) {
                // register attach parameters
                registry.push([selector, events, behavior, options]);
                if(jQuery.isReady) {
                    jQuery('[data-'+selector+']').each(function() {
                        var element = jQuery(this);
                        attach(element, undefined, events, behavior, options);
                        getInstance(element, behavior, options);
                    });
                }
            } else {
                console.warn('can not eager bind without selector');
            }
        } else {
            // lazy + delegate || jQuery object
            var attachArgs = [root, selector, events, behavior, options];

            if(jQuery.isReady) {
                attach.apply(null,attachArgs);
            } else {
                attachQueue.push(attachArgs);
            }
        }

    };

    function eagerLoad(event) {

        var target = event.target ? jQuery(event.target) : jQuery('body'); // trigger('elementInsert') || jQuery() hivta

        jQuery.each(registry, function(i, entry) {
            // selector, events, behavior, options
            var count = target.find(entry[0]).add(target.filter(entry[0])).each(function() {
                var element = jQuery(this);
                //bindAll(element, '', entry[1], entry[2], entry[3]);
                attach(element, undefined, entry[1], entry[2], entry[3]);
                getInstance(element, entry[2], entry[3]);
            }).length;

            // stat it
            if(debug) vc_debug._eager(entry[0], count);
        });
    }

    function attach(root, selector, events, behavior, options) {
        jQuery.each(events, function(filter, handlers) {
            var types = '';
            jQuery.each(handlers, function(type) {
                types += type + ' ';
            });

            var data = {
                behavior: behavior,
                handlers: handlers,
                options: options,
                selector: selector
            };

            if(selector !== undefined || filter !== '') {
                // delegate
                jQuery(root).delegate((selector !== undefined ? selector + ' ' + filter : filter), types, data, delegateHandler);
            } else {
                // bind
                jQuery(root).bind(types, data, delegateHandler);
            }

            if(debug) vc_debug._attach(root, (selector !== undefined ? selector + ' ' + filter : filter), types, options);
        });
    }

    function delegateHandler(event) {
        var element =
            event.data.selector !== undefined ? jQuery(event.currentTarget).closest(event.data.selector) : jQuery(event.liveFired || event.delegateTarget || event.currentTarget);
        return event.data.handlers[event.type].apply(getInstance(element, event.data.behavior, event.data.options), arguments);
    }

    //thanks to Papa Crockford
    function object(o) {
        function F() {}
        F.prototype = o;
        return new F();
    }

    var behaviorIds = 0;

    function getInstance(element, behavior, options) {
        var id = behavior._attach_id || (behavior._attach_id = ++behaviorIds);
        var instances = element.data('instances');
        if(! instances) {
            instances = {};
            element.data('instances', instances);
        }
        var instance = instances[id];
        if(! instance) {
            if(typeof behavior === 'function') {
                instance = new behavior(element, options);
            } else if (typeof behavior.initialize === 'function') {
                instance = object(behavior); //TODO: cache created constructors
                instance.initialize(element, options);
            } else {
                instance = {};
            }
            instances[id] = instance;
        }
        return instance;
    }

})();
