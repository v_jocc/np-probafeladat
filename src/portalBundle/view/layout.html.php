<!DOCTYPE html>

<html lang="hu">
    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <link rel="icon" href="" type="image/png">

        <?php

        assets('Style','header');
        assets('Js','header');

        ?>

        <title><?=DEFAULT_SITE_TITLE?></title>

	</head>

	<body>

        <div id="content" data-json-path="/web/assets/data/">

            <?php

            // header menu
            requireFile(BUNDLE_VIEW_PATH.'/partials/header.html.php', $sansa);

            ?>

            <div class="content-wrapper">
                <?php

                // page modules
                requireFile(APP_PATH.'/manager/modulePortalRoute.php', $sansa);

                ?>
            </div>

        </div>

	</body>
</html>
