<header>
    <nav>
        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav-tab row">
                    <li class="col-sm-3 col-xs-6">
                        <a class="header-tab purple active" data-ticket="tickets" href="#">
                            <span class="tab-text">Entry tickets</span>
                            <span class="np-icon icon-ticket"></span>
                        </a>
                    </li>
                    <li class="col-sm-3 col-xs-6">
                        <a class="header-tab yellow" data-ticket="accessories" href="#">
                            <span class="tab-text">Accessories</span>
                            <span class="np-icon icon-shopping_cart_content"></span>
                        </a>
                    </li>
                    <li class="col-sm-3 col-xs-6">
                        <a class="header-tab green" data-ticket="food_services" href="#">
                            <span class="tab-text">Food services</span>
                            <span class="np-icon icon-fast_food"></span>
                        </a>
                    </li>
                    <li class="col-sm-3 col-xs-6">
                        <a class="header-tab blue" data-ticket="vip_programs" href="#">
                            <span class="tab-text">VIP programs</span>
                            <span class="np-icon icon-microphone_2"></span>
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
</header>