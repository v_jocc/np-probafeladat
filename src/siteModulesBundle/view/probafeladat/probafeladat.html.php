<section class="tickets-wrapper">

    <div class="ticket-element">
        <div class="row">
            <div class="col-sm-8 col-xs-12 ticket-left">
                <h2>title</h2>
                <div class="prices-info">
                    <ul>
                    </ul>
                </div>
                <div class="info-list">
                    <ul>
                    </ul>
                </div>
                <div class="services-wrapper">
                    <div class="service">
                        <div class="service-icon"></div>
                        <div class="service-title"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12 ticket-right">
                    <div class="old-price"></div>
                    <div class="price"></div>
                    <div class="basket-info"></div>
                    <div class="basket-functions">
                        <button class="add-to-basket">
                            <span class="icon-shopping_cart"></span>
                            <span class="basket-text">Add to basket</span>
                        </button>
                        <div class="time-left">
                            time left: <span></span>
                        </div>
                    </div>

            </div>
        </div>
    </div>

</section>




