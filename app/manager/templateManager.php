<?php

if(!isset($_GET['ajaxRequest'])) {

    requireFile(BUNDLE_VIEW_PATH.'/layout.html.php', $sansa);

} else {

    $ajaxRequest = $_GET['ajaxRequest'];
    $ajaxRequest($sansa, []);

}
