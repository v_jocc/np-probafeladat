<?php

function assets($type, $space) {
    requireFile(BUNDLE_ASSETS_PATH.'/'.$space.$type.'Assets.html.php', null);
}

function notFound($sansa) {
    requireFile(BUNDLE_VIEW_PATH.'/default/404.html.php', $sansa);
}