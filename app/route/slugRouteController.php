<?php

require_once(APP_PATH.'/route/url.php');

class SansaRoute {

    //vissza térek az url szegmensekkel, így a $slug változóba a szegmensek is benne lesznek.
    public function segment($segment) {
        $sansaUrl = new sansaUrl('/admin');

        return $sansaUrl->segment($segment);

    }

    //megnézem mi az első szegmens, hogy a megfelelő modul töltődjön be.
    public function segmentOne() {

        if (!$this->segment(1)) {
            $page = 'home';
        } else {
            $page = $this->segment(1);
        }

        return $page;

    }

    public function activeLink() {

        $segment = [];

        for ($x = 1; $x <= 10; $x++) {
            if($this->segment($x) != '') {
                $segment[$x] = $this->segment($x);
            }
        }

        $activeLink = implode('/', $segment);

        return $activeLink;

    }

}
