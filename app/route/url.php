<?php

class sansaUrl {
	var $pagePath;

	function __construct($pagePath) {

		$this->pagePath = $this->removeSlash($pagePath);
	}

	function __toString() {

		return $this->pagePath;
	}

	private function removeSlash($string) {

		if ( $string[strlen($string) - 1] == '/' ) {
			$string = rtrim($string, '/');
		}

		return $string;
	}

	function segment($segment) {

		$url = str_replace($this->pagePath, '', $_SERVER['REQUEST_URI']);
		$url = explode('/', $url);

		if (isset($url[$segment])) {
			return $url[$segment];
		} else {
			return false;
		}
	}

	private function addSlash($string) {
		
		if ( $string[strlen($string) - 1] != '/' ) {
			$string .= '/';
		}

		return $string;
	}


}

