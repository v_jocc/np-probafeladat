<?php

DEFINE('VENDOR_PATH', 'vendor');
DEFINE('APP_PATH', 'app');
DEFINE('CONTROLLER', '/controller/controller.php');
DEFINE('BUNDLE_VIEW_PATH', BUNDLE.'/view');
DEFINE('BUNDLE_SERVICES_PATH', BUNDLE.'/services');
DEFINE('BUNDLE_ASSETS_PATH', BUNDLE.'/view/partials/bundleAssets');

DEFINE('MODULE_BUNDLE_PATH', 'src/siteModulesBundle');
DEFINE('PORTAL_BUNDLE_PATH', 'src/portalBundle');

DEFINE('ASSETIC_PATH', 'web/assets');
DEFINE('UPLOADS_PATH', 'web/assets/uploads/images');
DEFINE('THUMBS_PATH', 'web/assets/uploads/thumbs');

DEFINE('DEFAULT_SITE_TITLE', 'Probafeladat');
