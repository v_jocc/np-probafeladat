<?php

require APP_PATH.'/route/slugRouteController.php';
require PORTAL_BUNDLE_PATH.CONTROLLER.'';
require MODULE_BUNDLE_PATH.CONTROLLER.'';

class sansaController {

    public function form() {

        $form = new formBuilder();
        return $form;

    }

    public function slug() {

        $slug = new SansaRoute;
        return $slug;

    }

    public function portalView() {

        $portalView = new portalController;
        return $portalView;

    }

    public function moduleView() {

        $moduleView = new moduleController;
        return $moduleView;

    }

}
