<?php

class formBuilder {
    protected $_attributes = array();

    public function text($label, $type, $array) {

        if($array) {

            $input = '<input type="'.$type.'" ';

            echo '<div class="form-group">';
            echo '<label>'.$label.'</label>';
                echo $input;
                    foreach($array as $a => $b) {
                        echo $a,'="',$b,"\"\n";
                    }
                echo ' />';
            echo '</div>';

        }

    }

    public function checkbox($label, $value, $array) {

        if($array) {

            $input = '<input type="checkbox"';
            echo '<div class="form-group no-margin">';
            echo '<label class="checkbox">';
                echo $input;

                if ($value) {
                    echo ' checked="checked "';
                }

                foreach($array as $a => $b) {
                    echo $a,'="',$b,"\"\n";
                }

                echo ' />';
            echo $label;
            echo '</label>';
            echo '</div>';

        }

    }

    public function select($label, $value, $options, $array) {

        $default = false;

        if(isset($array["data-default"])) {
            $default = $array["data-default"];
        }

        if($array) {

            $select = '<select ';

            echo '<div class="form-group">';
            echo '<label>'.$label.'</label>';
            echo $select;
            foreach($array as $a => $b) {
                echo $a,'="',$b,"\"\n";
            }
            echo '>';
            echo '<option default></option>';
            if(is_array($options)) {
                foreach($options as $thisValue => $text) {

                    echo '<option value="'.$thisValue.'" ';   //.$b.'</option>';

                    if (($thisValue == $value) || (($default != false) && ($default == $thisValue))) {
                        echo ' selected="selected "';
                    }

                    echo '>'.$text.'</option>';

                }
            }

            echo '</select>';
            echo '</div>';

        }

    }

    public function selectWithValue($label, $value, $options, $array) {

        if($array) {

            $select = '<select ';

            echo '<div class="form-group">';
            echo '<label>'.$label.'</label>';
            echo $select;
            foreach($array as $a => $b) {
                echo $a,'="',$b,"\"\n";
            }
            echo '>';
            echo '<option default></option>';
            if(is_array($options)) {
                foreach($options as $thisValue => $text) {

                    echo '<option value="'.$text.'" ';   //.$b.'</option>';
                    if ($text == $value) {
                        echo ' selected="selected "';
                    }

                    echo '>'.$text.'</option>';

                }
            }

            echo '</select>';
            echo '</div>';

        }

    }

    public function textarea($label, $value, $array) {

        if($array) {

            $input = '<textarea ';

            echo '<div class="form-group">';
            echo '<label>'.$label.'</label>';
            echo $input;
            foreach($array as $a => $b) {
                echo $a,'="',$b,"\"\n";
            }
            echo '>'.$value.'</textarea>';
            echo '</div>';

        }

    }

    public function submit($value, $array) {

        if($array) {

            $submit = '<input type="submit" value="'.$value.'"';

            echo '<div class="form-group no-margin pull-right">';
            echo $submit;
            foreach($array as $a => $b) {
                echo $a,'="',$b,"\"\n";
            }
            echo ' />';
            echo '</div>';

        }

    }

}