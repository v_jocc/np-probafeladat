<?php

$activeBundle = 'portalBundle';

DEFINE('ACTIVE_BUNDLE', $activeBundle);
DEFINE('BUNDLE', 'src/'.$activeBundle);

requireFile('app/config.php', null);
requireFile(APP_PATH.'/controller.php', null);

$sansa = new sansaController;

requireFile(APP_PATH.'/services.php', $sansa);

requireFile(APP_PATH.'/manager/templateManager.php', $sansa);

function requireFile($file, $sansa) {

    require $file.'';

}